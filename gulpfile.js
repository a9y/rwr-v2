'use strict'

var gulp = require('gulp'),
    gulpLoadPlugins = require('gulp-load-plugins');

const $ = gulpLoadPlugins()

gulp.task('images', () => {
    return gulp.src('assets/photos/*.{png,jpg,jpeg,gif,svg,webp}')
        .pipe($.print())
        .pipe($.imagemin())
        .pipe(gulp.dest('images/photos'));
});

gulp.task('thumbs', function () {
    gulp.src('assets/photos/*.{png,jpg,jpeg,gif,svg,webp}')
      .pipe($.imageResize({
        width : 400,
        upscale : true
      }))
      .pipe(gulp.dest('images/thumbs'));
  })