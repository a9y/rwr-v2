var config = {
    apiKey: "AIzaSyDtLwLQzQI78-6ZWCAMBeKoZ2IGuDyyPZo",
    authDomain: "ridewithray-6358d.firebaseapp.com",
    databaseURL: "https://ridewithray-6358d.firebaseio.com",
    projectId: "ridewithray-6358d",
    storageBucket: "ridewithray-6358d.appspot.com",
    messagingSenderId: "246122832371"
  };

firebase.initializeApp(config);

var db = firebase.database();

function addMessage(msg) {
	db.ref('messages').push({
		timestamp: Date.now(),
		name: msg.name,
		email: msg.email,
		phone: msg.phone,
		date: msg.date,
		message: msg.message
	})
}